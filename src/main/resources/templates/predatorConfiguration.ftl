<html>
  <head>
    <title>[@ww.text name='predator' /]</title>
    <meta name="decorator" content="adminpage">
  </head>
  <body>
     <h1>[@ww.text name='predator' /]</h1>

     [@ww.form action="updatePredatorPlugin.action" method="post"
            submitLabelKey='predator.config.save']
         [@ww.text name='predator.config.description'/]
         <br/><br/>
         [@ww.textfield labelKey='predator.config.agents' name="agentsRegex" cssClass="long-field" required=true /]

         [@ww.checkbox labelKey="predator.config.fail" name="failBuild"/]

         [@ui.bambooSection titleKey='predator.config.commands']
             [@ww.textfield labelKey='predator.config.commands.run' name="commandsToRun" cssClass="long-field"/]
         [/@ui.bambooSection]

         [@ui.bambooSection titleKey='predator.config.processes']
            [@ww.textfield labelKey='predator.config.processes.kill' name="processesToKillRegex" cssClass="long-field"/]
         [/@ui.bambooSection]

         [@ui.bambooSection titleKey='predator.config.remove']
            [@ww.textfield labelKey='predator.config.remove.files' name="deleteFiles" cssClass="long-field"/]
            [@ww.checkbox labelKey='predator.config.remove.directories' name="removeBuildWorkingDir" toggle="true"/]
            [@ui.bambooSection dependsOn="removeBuildWorkingDir" showOn="true"]
                [@ww.checkbox labelKey='predator.config.remove.keep.activebranches' name="keepBuildWorkingDirActiveBranches"/]
            [/@ui.bambooSection]
            [@ww.checkbox labelKey="predator.config.remove.snapshots" name="deleteSnapshotsMaven"/]

            [@ww.checkbox labelKey="predator.config.remove.stable" name="deleteStableMaven" toggle='true'/]
            [@ui.bambooSection dependsOn="deleteStableMaven" showOn="true"]
                [@ww.textfield labelKey="predator.config.remove.stable.age" name="ageDaysStableToDelete"
                cssClass="short-field" required=true /]
                [@ww.textfield labelKey="predator.config.remove.stable.minimum" name="minimumFreeDiskGB"
                cssClass="short-field" required=true /]
            [/@ui.bambooSection]
        [/@ui.bambooSection]
     [/@ww.form]
  </body>
</html>