package com.atlassian.bamboo.plugin.predator;


import com.google.common.collect.Lists;

import java.util.List;

public class PredatorConstants {

    public static final String KEY_AGENTS_REGEX = "agents";
    public static final String KEY_FAIL_BUILD = "fail";
    public static final String KEY_PROCESSES = "processes";
    public static final String KEY_COMMANDS = "commands";
    public static final String KEY_DELETE_WORK_DIR = "delete.workingdir";
    public static final String KEY_KEEP_WORK_DIR_ACTIVE_BRANCHES = "keep.workingdir.activebranches";
    public static final String KEY_ACTIVE_PLAN_KEYS = "keep.workingdir.activebranches.list";
    public static final String KEY_DELETE_FILES = "delete.files";
    public static final String KEY_DELETE_SNAPSHOT = "delete.snapshot";
    public static final String KEY_DELETE_STABLE = "delete.stable";
    public static final String KEY_AGE_STABLE = "delete.stable.age";
    public static final String KEY_FREE_DISK_STABLE = "delete.stable.free";
    public static final String CUSTOM_VAR_PREFIX = "custom.";
    public static final String KEY_PLAN_OVERRIDE = CUSTOM_VAR_PREFIX + "predator.disable";
    public static final String BANDANA_PREFIX = "com.atlassian.bamboo.plugin.predator.";
    public static final String MAVEN_LOCAL_REPO = "/.m2/repository/";
    public static final List<String> BUILD_FOLDERS = Lists.newArrayList("_git-repositories-cache", "_hg-repositories-cache", "repositoryData");

    public static final String DEFAULT_AGENTS = ".*";
    public static final String LOG_PREFIX = "[PredatorPreBuildAction] ";
}
