package com.atlassian.bamboo.plugin.predator.executor;


import com.atlassian.bamboo.chains.StageExecution;
import com.atlassian.bamboo.chains.plugins.PreJobAction;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChainBranch;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.plugin.predator.PredatorConstants;
import com.atlassian.bamboo.plugin.predator.PredatorPluginService;
import com.atlassian.bamboo.v2.build.BuildContext;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Simple action that gets the global config from the plugin service and stores it in the build context
 */
public class PredatorPreJobAction implements PreJobAction
{

    //------------------------------------------------------------------------------------------------------------------
    //Members
    private PredatorPluginService predatorPluginService;
    private static final Logger log = Logger.getLogger(PredatorPreBuildAction.class);
    private CachedPlanManager cachedPlanManager;

    //------------------------------------------------------------------------------------------------------------------
    //Interface Methods
    @Override
    public void execute(@NotNull StageExecution stageExecution, @NotNull BuildContext buildContext)
    {
        final Map<String, String> customConfiguration = buildContext.getBuildDefinition().getCustomConfiguration();

        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_AGENTS_REGEX, predatorPluginService.getAgentsRegex());
        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_FAIL_BUILD, predatorPluginService.getFailBuild().toString());
        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_PROCESSES, predatorPluginService.getProcessesToKillRegex());
        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_COMMANDS, predatorPluginService.getCommandsToRun());
        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_DELETE_WORK_DIR, predatorPluginService.getRemoveBuildWorkingDir().toString());
        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_KEEP_WORK_DIR_ACTIVE_BRANCHES, predatorPluginService.getKeepBuildWorkingDirActiveBranches().toString());
        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_DELETE_SNAPSHOT, predatorPluginService.getDeleteSnapshotsMaven().toString());
        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_DELETE_STABLE, predatorPluginService.getDeleteStableMaven().toString());
        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_AGE_STABLE, predatorPluginService.getAgeDaysStableToDelete().toString());
        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_FREE_DISK_STABLE, predatorPluginService.getMinimumFreeDiskGB().toString());
        customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_DELETE_FILES, predatorPluginService.getDeleteFiles());

        if (predatorPluginService.getKeepBuildWorkingDirActiveBranches()) {
            customConfiguration.put(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_ACTIVE_PLAN_KEYS, StringUtils.join(getActivePlanKeys(), " "));
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    //Private Methods
    private String[] getActivePlanKeys() {
        List<String> activePlanKeys = new ArrayList<>();
        List<ImmutableTopLevelPlan> plans = cachedPlanManager.getPlans();
        for (ImmutableTopLevelPlan plan : plans) {
            activePlanKeys.add(plan.getKey());
            List<ImmutableChainBranch> branches = cachedPlanManager.getBranchesForChain(plan);
            for(ImmutableChainBranch branch : branches) {
                if (!branch.isSuspendedFromBuilding()) {
                    activePlanKeys.add(branch.getKey());
                }
            }
        }

        return activePlanKeys.toArray(new String[0]);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Injectors
    public void setPredatorPluginService(PredatorPluginService predatorPluginService)
    {
        this.predatorPluginService = predatorPluginService;
    }

    public void setCachedPlanManager(CachedPlanManager cachedPlanManager) {
        this.cachedPlanManager = cachedPlanManager;
    }
}
