package com.atlassian.bamboo.plugin.predator;

/**
 * Spring component that stores/allows access to the global predator config
 */
public interface PredatorPluginService {

    public void setAgentsRegex(String agentsRegex);

    public void setFailBuild(Boolean failBuild);

    public void setProcessesToKillRegex(String processesToKillRegex);

    public void setCommandsToRun(String commandsToRun);

    public void setRemoveBuildWorkingDir(Boolean removeBuildWorkingDir);

    public void setKeepBuildWorkingDirActiveBranches(Boolean keepBuildWorkingDirActiveBranches);

    public void setDeleteSnapshotsMaven(Boolean deleteSnapshotsMaven);

    public void setDeleteStableMaven(Boolean deleteStableMaven);

    public void setAgeDaysStableToDelete(Float ageStableToDelete);

    public void setMinimumFreeDiskGB(Float minimumFreeDisk);

    public void setDeleteFiles(String files);

    public String getAgentsRegex();

    public Boolean getFailBuild();

    public String getProcessesToKillRegex();

    public String getCommandsToRun();

    public Boolean getRemoveBuildWorkingDir();

    public Boolean getKeepBuildWorkingDirActiveBranches();

    public Boolean getDeleteSnapshotsMaven();

    public Boolean getDeleteStableMaven();

    public Float getAgeDaysStableToDelete();

    public Float getMinimumFreeDiskGB();

    public String getDeleteFiles();
}
