package com.atlassian.bamboo.plugin.predator.config;

import com.atlassian.bamboo.configuration.GlobalAdminAction;
import com.atlassian.bamboo.plugin.predator.PredatorPluginService;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Predator configuration panel in the admin section, used to modify the global predator settings.
 *
 * Essentially a wrapper to the predator service.
 */
public class PredatorConfiguration extends GlobalAdminAction
{
    private final BambooAuthenticationContext bambooAuthenticationContext;
    private final BambooPermissionManager bambooPermissionManager;
    private final PredatorPluginService predatorPluginService;
    private String agentsRegex;
    private boolean failBuild;
    private String processesToKillRegex;
    private String commandsToRun;
    private boolean removeBuildWorkingDir;
    private boolean keepBuildWorkingDirActiveBranches;
    private boolean deleteSnapshotsMaven;
    private boolean deleteStableMaven;
    private String ageDaysStableToDelete;
    private String minimumFreeDiskGB;
    private String deleteFiles;



    public PredatorConfiguration(PredatorPluginService predatorPluginService,
                                 BambooAuthenticationContext bambooAuthenticationContext, BambooPermissionManager bambooPermissionManager)
    {
        this.bambooAuthenticationContext = bambooAuthenticationContext;
        this.bambooPermissionManager = bambooPermissionManager;
        this.predatorPluginService = predatorPluginService;
    }

    @Override
    public String doExecute() throws Exception {
        final User user = bambooAuthenticationContext.getUser();
        if (user != null && bambooPermissionManager.isAdmin(user.getName()))
        {
            return super.doExecute();
        }
        else
        {
            return ERROR;
        }
    }

    @Override
    public String doDefault() {

        agentsRegex = predatorPluginService.getAgentsRegex();
        failBuild = predatorPluginService.getFailBuild();
        processesToKillRegex = predatorPluginService.getProcessesToKillRegex();
        commandsToRun = predatorPluginService.getCommandsToRun();
        removeBuildWorkingDir = predatorPluginService.getRemoveBuildWorkingDir();
        keepBuildWorkingDirActiveBranches = predatorPluginService.getKeepBuildWorkingDirActiveBranches();
        deleteSnapshotsMaven = predatorPluginService.getDeleteSnapshotsMaven();
        deleteStableMaven = predatorPluginService.getDeleteStableMaven();
        ageDaysStableToDelete = predatorPluginService.getAgeDaysStableToDelete().toString();
        minimumFreeDiskGB = predatorPluginService.getMinimumFreeDiskGB().toString();
        deleteFiles = predatorPluginService.getDeleteFiles();

        return SUCCESS;
    }

    public String doEdit() {
        validate();

        if (hasAnyErrors()){
            return ERROR;
        }

        predatorPluginService.setAgentsRegex(agentsRegex);
        predatorPluginService.setFailBuild(failBuild);
        predatorPluginService.setProcessesToKillRegex(processesToKillRegex);
        predatorPluginService.setCommandsToRun(commandsToRun);
        predatorPluginService.setRemoveBuildWorkingDir(removeBuildWorkingDir);
        predatorPluginService.setKeepBuildWorkingDirActiveBranches(keepBuildWorkingDirActiveBranches);
        predatorPluginService.setDeleteSnapshotsMaven(deleteSnapshotsMaven);
        predatorPluginService.setDeleteStableMaven(deleteStableMaven);
        predatorPluginService.setDeleteFiles(deleteFiles);
        if (deleteStableMaven) {
            predatorPluginService.setAgeDaysStableToDelete(Float.parseFloat(ageDaysStableToDelete));
            predatorPluginService.setMinimumFreeDiskGB(Float.parseFloat(minimumFreeDiskGB));
        }

        addActionMessage(getText("predator.plan.config.success"));
        return SUCCESS;
    }



    @Override
    public void validate() {
        super.validate();

        if (StringUtils.isEmpty(agentsRegex) ){
            addFieldError("agentsRegex", getText("predator.error.empty"));
        } else {
            try {
                Pattern.compile(agentsRegex);
            } catch (PatternSyntaxException e) {
                addFieldError("agentsRegex", getText("predator.error.regex"));
            }
        }

        if (!processesToKillRegex.isEmpty()) {
            try {
                Pattern.compile(processesToKillRegex);
            } catch (PatternSyntaxException e) {
                addFieldError("processesToKillRegex", getText("predator.error.regex"));
            }
        }

        if (deleteStableMaven) {
            if (StringUtils.isEmpty(ageDaysStableToDelete) ){
                addFieldError("ageDaysStableToDelete", getText("predator.error.empty"));
            } else {
                try {
                    Float.parseFloat(ageDaysStableToDelete);
                } catch (NumberFormatException e) {
                    addFieldError("ageDaysStableToDelete", getText("predator.error.number"));
                }
            }

            if (StringUtils.isEmpty(minimumFreeDiskGB) ){
                addFieldError("minimumFreeDiskGB", getText("predator.error.empty"));
            } else {
                try {
                    Float.parseFloat(minimumFreeDiskGB);
                } catch (NumberFormatException e) {
                    addFieldError("minimumFreeDiskGB", getText("predator.error.number"));
                }
            }
        }


    }

    public void setAgentsRegex(String agentsRegex) {
        this.agentsRegex = agentsRegex;
    }

    public void setFailBuild(boolean failBuild) {
        this.failBuild = failBuild;
    }

    public void setProcessesToKillRegex(String processesToKillRegex) {
        this.processesToKillRegex = processesToKillRegex;
    }

    public void setCommandsToRun(String commandsToRun) {
        this.commandsToRun = commandsToRun;
    }

    public void setRemoveBuildWorkingDir(boolean removeBuildWorkingDir) {
        this.removeBuildWorkingDir = removeBuildWorkingDir;
    }

    public void setKeepBuildWorkingDirActiveBranches(boolean keepBuildWorkingDirActiveBranches) {
        this.keepBuildWorkingDirActiveBranches = keepBuildWorkingDirActiveBranches;
    }

    public void setDeleteSnapshotsMaven(boolean deleteSnapshotsMaven) {
        this.deleteSnapshotsMaven = deleteSnapshotsMaven;
    }

    public void setDeleteStableMaven(boolean deleteStableMaven) {
        this.deleteStableMaven = deleteStableMaven;
    }

    public void setAgeDaysStableToDelete(String ageDaysStableToDelete) {
        this.ageDaysStableToDelete = ageDaysStableToDelete;
    }

    public void setMinimumFreeDiskGB(String minimumFreeDiskGB) {
        this.minimumFreeDiskGB = minimumFreeDiskGB;
    }

    public String getAgentsRegex() {
        return agentsRegex;
    }

    public boolean isFailBuild() {
        return failBuild;
    }

    public String getProcessesToKillRegex() {
        return processesToKillRegex;
    }

    public String getCommandsToRun() {
        return commandsToRun;
    }

    public boolean isRemoveBuildWorkingDir() {
        return removeBuildWorkingDir;
    }

    public boolean isKeepBuildWorkingDirActiveBranches() { return keepBuildWorkingDirActiveBranches; }

    public boolean isDeleteSnapshotsMaven() {
        return deleteSnapshotsMaven;
    }

    public boolean isDeleteStableMaven() {
        return deleteStableMaven;
    }

    public String getAgeDaysStableToDelete() {
        return ageDaysStableToDelete;
    }

    public String getMinimumFreeDiskGB() {
        return minimumFreeDiskGB;
    }

    public String getDeleteFiles() {
        return deleteFiles;
    }

    public void setDeleteFiles(String deleteFiles) {
        this.deleteFiles = deleteFiles;
    }
}
