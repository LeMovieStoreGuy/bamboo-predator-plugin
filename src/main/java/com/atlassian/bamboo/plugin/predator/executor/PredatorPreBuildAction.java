package com.atlassian.bamboo.plugin.predator.executor;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.CustomPreBuildAction;
import com.atlassian.bamboo.build.fileserver.BuildDirectoryManager;
import com.atlassian.bamboo.plugin.predator.PredatorConstants;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.agent.capability.AgentContext;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Map;


/**
 * Pre Build Action that shells out and calls predator
 */
public class PredatorPreBuildAction implements CustomPreBuildAction
{
    //-----------------------------—-------------------------------------------------------------------------------------
    //Members
    private static final Logger log = Logger.getLogger(PredatorPreBuildAction.class);
    private BuildContext buildContext;
    private BuildLoggerManager buildLoggerManager;
    private BuildDirectoryManager buildDirectoryManager;
    private AgentContext agentContext;
    private String agentsRegex;
    private String deleteFiles;
    private boolean failBuild;
    private String processesToKillRegex;
    private String commandsToRun;
    private File mavenLocalRepoDirectory;
    private boolean removeBuildWorkingDir;
    private boolean keepBuildWorkingDirActiveBranches;
    private boolean deleteSnapshotsMaven;
    private boolean deleteStableMaven;
    private Float ageStableToDelete;
    private Float minimumFreeDisk;
    private File agentWorkingDir;
    private String jobKey;
    private boolean disabled;
    private String[] activePlanKeys;

    //------------------------------------------------------------------------------------------------------------------
    //Interface Methods
    @Override
    public ErrorCollection validate(BuildConfiguration buildConfiguration)
    {
        return null;
    }

    @Override
    public void init(@NotNull BuildContext buildContext)
    {
        log.setLevel(Level.ALL);
        this.buildContext = buildContext;
        if (isRunningInDocker()) {
            return;
        }
        this.jobKey = this.buildContext.getPlanKey();
        this.buildLoggerManager = ComponentUtils.getComponentIfNull(this.buildLoggerManager, "buildLoggerManager");
        this.agentContext = ComponentUtils.getComponentIfNull(this.agentContext, "agentContext");
        this.agentWorkingDir = new File(this.buildDirectoryManager.getWorkingDirectoryOfCurrentAgent().getAbsolutePath());
        this.mavenLocalRepoDirectory = new File(FileUtils.getUserDirectory(), PredatorConstants.MAVEN_LOCAL_REPO);

        final Map<String, String> customConfiguration = this.buildContext.getBuildDefinition().getCustomConfiguration();
        this.agentsRegex = customConfiguration.get(PredatorConstants.CUSTOM_VAR_PREFIX + PredatorConstants.KEY_AGENTS_REGEX);
        this.failBuild = Boolean.parseBoolean(getCustomVariable(customConfiguration, PredatorConstants.KEY_FAIL_BUILD));
        this.processesToKillRegex = getCustomVariable(customConfiguration, PredatorConstants.KEY_PROCESSES);
        this.commandsToRun = getCustomVariable(customConfiguration, PredatorConstants.KEY_COMMANDS);
        this.removeBuildWorkingDir = Boolean.parseBoolean(getCustomVariable(customConfiguration, PredatorConstants.KEY_DELETE_WORK_DIR));
        this.keepBuildWorkingDirActiveBranches = Boolean.parseBoolean(getCustomVariable(customConfiguration, PredatorConstants.KEY_KEEP_WORK_DIR_ACTIVE_BRANCHES));
        this.deleteSnapshotsMaven = Boolean.parseBoolean(getCustomVariable(customConfiguration, PredatorConstants.KEY_DELETE_SNAPSHOT));
        this.deleteStableMaven = Boolean.parseBoolean(getCustomVariable(customConfiguration, PredatorConstants.KEY_DELETE_STABLE));
        this.ageStableToDelete = Float.parseFloat(getCustomVariable(customConfiguration, PredatorConstants.KEY_AGE_STABLE));
        this.minimumFreeDisk = Float.parseFloat(getCustomVariable(customConfiguration, PredatorConstants.KEY_FREE_DISK_STABLE));
        this.deleteFiles = getCustomVariable(customConfiguration, PredatorConstants.KEY_DELETE_FILES);
        if (this.keepBuildWorkingDirActiveBranches) {
            this.activePlanKeys = StringUtils.split(getCustomVariable(customConfiguration, PredatorConstants.KEY_ACTIVE_PLAN_KEYS));
        }

        this.disabled = Boolean.parseBoolean(this.buildContext.getParentBuildContext().getBuildDefinition().getCustomConfiguration().get(PredatorConstants.KEY_PLAN_OVERRIDE));
    }

    private String getCustomVariable(Map<String, String> customConfiguration, final String key) {
        return customConfiguration.get(PredatorConstants.CUSTOM_VAR_PREFIX + key);
    }

    @NotNull
    @Override
    public BuildContext call() {
        if (isRunningInDocker()) {
            return buildContext;
        }
        
        final PredatorExecutor predatorExecutor = new PredatorExecutor(buildLoggerManager.getLogger(this.buildContext.getResultKey()));

        predatorExecutor.logOutput("Starting predator plugin, pre build agent cleaner");

        if (this.disabled) {
            predatorExecutor.logOutput("Agent cleaning has been disabled for this plan, skipping");
            return this.buildContext;
        }

        final String agentName = agentContext.getBuildAgent().getName();
        if (!predatorExecutor.shouldRunInThisAgent(agentName, agentsRegex)){
            predatorExecutor.logOutput("Skipping agent cleaning as " + agentName + " does not match " + agentsRegex);
            return this.buildContext;
        }

        boolean failed = false;
        try {
            predatorExecutor.logOutput(String.format("Running these commands: %s", commandsToRun));
            predatorExecutor.runCommands(StringUtils.split(commandsToRun, ';'));
        } catch (Exception e){
            predatorExecutor.logError("Exception when trying to run commands", e);
            failed = true;
        }

        try {
            predatorExecutor.killProcesses(processesToKillRegex);
        } catch (Exception e){
            predatorExecutor.logError("Exception when trying to kill processes", e);
            failed = true;
        }

        predatorExecutor.logOutput("Initial free space: " + predatorExecutor.retrieveFreeSpaceGB() + " GB");
        try {
            predatorExecutor.removeOldBuildWorkingDirectory(removeBuildWorkingDir, keepBuildWorkingDirActiveBranches, agentWorkingDir, jobKey, activePlanKeys);
        } catch (Exception e){
            predatorExecutor.logError("Exception when trying to remove old build working directories", e);
            failed = true;
        }

        try {
            predatorExecutor.removeMavenSnapshots(deleteSnapshotsMaven, mavenLocalRepoDirectory);
        } catch (Exception e){
            predatorExecutor.logError("Exception when trying to remove snapshots", e);
            failed = true;
        }

        try
        {
            predatorExecutor.removeEmptyArtifacts(mavenLocalRepoDirectory);
        }
        catch (Exception e)
        {
            predatorExecutor.logError("Exception when trying to delete empty artifacts", e);
            failed = true;
        }

        try {
            predatorExecutor.removeMavenStable(deleteStableMaven, mavenLocalRepoDirectory, ageStableToDelete, minimumFreeDisk);
        } catch (Exception e){
            predatorExecutor.logError("Exception when trying to stable artifacts", e);
            failed = true;
        }


        try {
            predatorExecutor.deleteFiles(StringUtils.split(deleteFiles, ','), FileUtils.getUserDirectory(), false);
        } catch (Exception e){
            predatorExecutor.logError("Exception when trying to delete files", e);
            failed = true;
        }

        predatorExecutor.logOutput("Final free space: " + predatorExecutor.retrieveFreeSpaceGB() + " GB");


        if (failed) {
            if (failBuild) {
                this.buildContext.getErrorCollection().addErrorMessage(PredatorConstants.LOG_PREFIX + "Failed to execute agent cleaning. Failing the build.");
            } else {
                predatorExecutor.logOutput("There was an exception in the previous predator steps. Predator is configured to not fail the build.");
            }
        }


        predatorExecutor.logOutput("Done");
        return this.buildContext;
    }




    //------------------------------------------------------------------------------------------------------------------
    //Injectors

    public void setBuildLoggerManager(BuildLoggerManager buildLoggerManager)
    {
        this.buildLoggerManager = buildLoggerManager;
    }

    public void setBuildDirectoryManager(BuildDirectoryManager buildDirectoryManager)
    {
        this.buildDirectoryManager = buildDirectoryManager;
    }

    private boolean isRunningInDocker() {
        return new File("/.dockerenv").exists();
    }
}
