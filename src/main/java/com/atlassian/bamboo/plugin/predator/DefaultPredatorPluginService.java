package com.atlassian.bamboo.plugin.predator;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bandana.BandanaManager;
import org.apache.log4j.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

public class DefaultPredatorPluginService implements PredatorPluginService {
    private static final Logger log = Logger.getLogger(DefaultPredatorPluginService.class);
    private final BandanaManager bandanaManager;
    private String agentsRegex;
    private Boolean failBuild;
    private String processesToKillRegex;
    private String commandsToRun;
    private Boolean removeBuildWorkingDir;
    private Boolean keepBuildWorkingDirActiveBranches;
    private Boolean deleteSnapshotsMaven;
    private Boolean deleteStableMaven;
    private Float ageDaysStableToDelete;
    private Float minimumFreeDiskGB;
    private String deleteFiles;

    public DefaultPredatorPluginService(final BandanaManager bandanaManager) {
        this.bandanaManager = checkNotNull(bandanaManager);

        failBuild = (Boolean) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_FAIL_BUILD, false);
        agentsRegex = (String) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_AGENTS_REGEX, PredatorConstants.DEFAULT_AGENTS);
        processesToKillRegex = (String) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_PROCESSES, "");
        commandsToRun = (String) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_COMMANDS, "");
        removeBuildWorkingDir = (Boolean) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_DELETE_WORK_DIR, false);
        keepBuildWorkingDirActiveBranches = (Boolean) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_KEEP_WORK_DIR_ACTIVE_BRANCHES, false);
        deleteSnapshotsMaven = (Boolean) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_DELETE_SNAPSHOT, false);
        deleteStableMaven = (Boolean) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_DELETE_STABLE, false);
        ageDaysStableToDelete = (Float) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_AGE_STABLE, 0f);
        minimumFreeDiskGB = (Float) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_FREE_DISK_STABLE, 0f);
        deleteFiles = (String) retrieveValueWithDefault(bandanaManager, PredatorConstants.KEY_DELETE_FILES, "");
    }


    public String getAgentsRegex() {
        return agentsRegex;
    }

    public void setAgentsRegex(String agentsRegex) {
        this.agentsRegex = agentsRegex;
        setBandanaValue(PredatorConstants.KEY_AGENTS_REGEX, agentsRegex);
    }

    public Boolean getFailBuild() {
        return failBuild;
    }

    public void setFailBuild(Boolean failBuild) {
        this.failBuild = failBuild;
        setBandanaValue(PredatorConstants.KEY_FAIL_BUILD, failBuild);
    }

    public String getProcessesToKillRegex() {
        return processesToKillRegex;
    }

    public void setProcessesToKillRegex(String processesToKillRegex) {
        this.processesToKillRegex = processesToKillRegex;
        setBandanaValue(PredatorConstants.KEY_PROCESSES, processesToKillRegex);
    }

    public String getCommandsToRun() {
        return commandsToRun;
    }

    public void setCommandsToRun(String commandsToRun) {
        this.commandsToRun = commandsToRun;
        setBandanaValue(PredatorConstants.KEY_COMMANDS, commandsToRun);
    }

    public Boolean getRemoveBuildWorkingDir() {
        return removeBuildWorkingDir;
    }

    @Override
    public Boolean getKeepBuildWorkingDirActiveBranches() {
        return keepBuildWorkingDirActiveBranches;
    }

    public void setRemoveBuildWorkingDir(Boolean removeBuildWorkingDir) {
        this.removeBuildWorkingDir = removeBuildWorkingDir;
        setBandanaValue(PredatorConstants.KEY_DELETE_WORK_DIR, removeBuildWorkingDir);
    }

    @Override
    public void setKeepBuildWorkingDirActiveBranches(Boolean keepBuildWorkingDirActiveBranches) {
        this.keepBuildWorkingDirActiveBranches = keepBuildWorkingDirActiveBranches;
        setBandanaValue(PredatorConstants.KEY_KEEP_WORK_DIR_ACTIVE_BRANCHES, keepBuildWorkingDirActiveBranches);
    }

    public Boolean getDeleteSnapshotsMaven() {
        return deleteSnapshotsMaven;
    }

    public void setDeleteSnapshotsMaven(Boolean deleteSnapshotsMaven) {
        this.deleteSnapshotsMaven = deleteSnapshotsMaven;
        setBandanaValue(PredatorConstants.KEY_DELETE_SNAPSHOT, deleteSnapshotsMaven);
    }

    public Boolean getDeleteStableMaven() {
        return deleteStableMaven;
    }

    public void setDeleteStableMaven(Boolean deleteStableMaven) {
        this.deleteStableMaven = deleteStableMaven;
        setBandanaValue(PredatorConstants.KEY_DELETE_STABLE, deleteStableMaven);
    }

    public Float getAgeDaysStableToDelete() {
        return ageDaysStableToDelete;
    }

    public void setAgeDaysStableToDelete(Float ageDaysStableToDelete) {
        this.ageDaysStableToDelete = ageDaysStableToDelete;
        setBandanaValue(PredatorConstants.KEY_AGE_STABLE, ageDaysStableToDelete);
    }

    public Float getMinimumFreeDiskGB() {
        return minimumFreeDiskGB;
    }

    public void setMinimumFreeDiskGB(Float minimumFreeDiskGB) {
        this.minimumFreeDiskGB = minimumFreeDiskGB;
        setBandanaValue(PredatorConstants.KEY_FREE_DISK_STABLE, minimumFreeDiskGB);
    }


    public String getDeleteFiles() {
        return deleteFiles;
    }

    public void setDeleteFiles(String deleteFiles) {
        this.deleteFiles = deleteFiles;
        setBandanaValue(PredatorConstants.KEY_DELETE_FILES, deleteFiles);
    }

    private void setBandanaValue(final String key, final Object value){
        bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, PredatorConstants.BANDANA_PREFIX + key, value);
    }

    private Object retrieveValueWithDefault(final BandanaManager bandanaManager, final String key, final Object defaultValue) {
        final Object bandanaObject = bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, PredatorConstants.BANDANA_PREFIX + key);
        Object retrievedObject;

        if (bandanaObject != null) {
            try {
                retrievedObject = bandanaObject;
            } catch (Exception e) {
                log.error("Unable to retrieve " + key + ", using default: " + defaultValue);
                bandanaManager.removeValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key);
                retrievedObject = defaultValue;
            }
        } else {
            retrievedObject = defaultValue;
        }
        return retrievedObject;
    }

}
